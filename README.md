# README #

"*The [`FUNCODES.CLUB`](http://www.funcodes.club) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.*"

## What is this repository for? ##

***The [`funcodes-meta`](https://bitbucket.org/funcodez/funcodes-meta) artifact is a meta artifact used for building the [`FUNCODES.CLUB`](http://www.funcodes.club) artifacts ([`club.funcodes`](https://bitbucket.org/funcodez) group) from scratch, usually used by developers. Note that the tools (a bunch of shell scripts) build around this meta artifact are not bundled with this meta artifact.***

## How do I get set up? ##

Clone all the repos by invoking `./clone-all.sh` for basic `HTTP` authentication or `./clone-all-ssh.sh` when using `SSH` authentication with public and provate keys.

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/funcodez/funcodes-meta/issues)
* Add a nifty user-interface
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`FUNCODES.CLUB`](http://www.funcodes.club) group of artifacts is published under some open source licenses; covered by the  [`funcodes-licensing`](https://bitbucket.org/funcodes/funcodes-licensing) ([`club.funcodes`](https://bitbucket.org/funcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
