# /////////////////////////////////////////////////////////////////////////////
# REFCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being covered by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////
#!/bin/bash

# /////////////////////////////////////////////////////////////////////////////
# This script tries to release all artifacts in the order as specified in  the 
# file "${DEPLOY_REACTOR}". Make sure you updated this file in case your maven 
# #dependencies of the top level artifacts changed! In case you want to run a
# meaningful dry run, please remove all artifacts from your home folder's 
# "~/.m2/repository" directory! Only then you can observe whether the 
# dependencies of the "${DEPLOY_REACTOR}" are in the right order and complete!   
# /////////////////////////////////////////////////////////////////////////////

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"
figlet -w 240 "/${PARENT_DIR}:>>>${SCRIPT_NAME}..."
MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f3- )"
if [ -z "${MODULE_NAME}" ]; then
	MODULE_NAME="$(echo -e "${SCRIPT_DIR}" | cut -d- -f2- )"
	if [ -z "${MODULE_NAME}" ]; then
		MODULE_NAME="${SCRIPT_DIR}"
	fi
fi
if [ -z ${COLUMNS} ] ; then
	export COLUMNS=$(tput cols)
fi

# ------------------------------------------------------------------------------
# ANSI ESCAPE CODES:
# ------------------------------------------------------------------------------

ESC_BOLD="\E[1m"
ESC_FAINT="\E[2m"
ESC_ITALIC="\E[3m"
ESC_UNDERLINE="\E[4m"
ESC_FG_RED="\E[31m"
ESC_FG_GREEN="\E[32m"
ESC_FG_YELLOW="\E[33m"
ESC_FG_BLUE="\E[34m"
ESC_FG_MAGENTA="\E[35m"
ESC_FG_CYAN="\E[36m"
ESC_FG_WHITE="\E[37m"
ESC_RESET="\E[0m"

# ------------------------------------------------------------------------------
# PRINTLN:
# ------------------------------------------------------------------------------

function printLn {
	char="-"
	if [[ $# == 1 ]] ; then
		char="$1"
	fi
	echo -en "${ESC_FAINT}"
	for (( i=0; i< ${COLUMNS}; i++ )) ; do
		echo -en "${char}"
	done
	echo -e "${ESC_RESET}"
}
# ------------------------------------------------------------------------------
# QUIT:
# ------------------------------------------------------------------------------

function quit {
	input=""
	while ([ "$input" != "q" ] && [ "$input" != "y" ]); do
		echo -ne "> Continue? Enter [${ESC_BOLD}q${ESC_RESET}] to quit, [${ESC_BOLD}y${ESC_RESET}] to continue: ";
		read input;
	done
	# printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
	if [ "$input" == "q" ] ; then
		printLn
		echo -e "> ${ESC_BOLD}Aborting due to user input.${ESC_RESET}"
		cd "${CURRENT_PATH}"
		exit 1
	fi
}

# ------------------------------------------------------------------------------
# BANNER:
# ------------------------------------------------------------------------------

function printBanner {
	banner=$( figlet -w 999 "/${MODULE_NAME}:>>>${SCRIPT_NAME}..." 2> /dev/null )
	if [ $? -eq 0 ]; then
		echo "${banner}" | cut -c -${COLUMNS}
	else
		banner "${SCRIPT_NAME}..." 2> /dev/null
		if [ $? -ne 0 ]; then
			echo -e "> ${SCRIPT_NAME}:" | tr a-z A-Z 
		fi
	fi
}

printBanner

# ------------------------------------------------------------------------------
# HELP:
# ------------------------------------------------------------------------------

function printHelp {
	printLn
	echo -e "Usage: ${ESC_BOLD}${SCRIPT_NAME}${ESC_RESET}.sh <fromVersion> <toVersion> | -h"
	printLn
	echo -e "Creates the markdown changelist for all GIT repositories fpund in the script's parent folder recursively."
	printLn
	echo -e "${ESC_BOLD}fromVersion${ESC_RESET}: The version from which to start"
	echo -e "${ESC_BOLD}  toVersion${ESC_RESET}: The version at which to end"
	echo -e "${ESC_BOLD}         -h${ESC_RESET}: Print this help"
	printLn
}

if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
	printHelp
	exit 0
fi

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

CONF_PATH="${SCRIPT_PATH}/meta.conf"
REACTOR_PATH=${SCRIPT_PATH}/reactor.txt
GROUP=$PARENT_DIR
VENDOR="${SCRIPT_DIR/-meta/}"
if [ -f "${CONF_PATH}" ]; then
  . ${CONF_PATH}
fi

fromVersion="$1"
toVersion="$2"
if [ -z "${fromVersion}" ] ; then
	printHelp
	echo -e "> ${ESC_BOLD}A from tag version is required as first argument (e.g. \"1.0.2\"). Aborting!${ESC_RESET}"
	exit 1
fi

if [ -z "${toVersion}" ] ; then
	printHelp
	echo -e "> ${ESC_BOLD}A to tag version is required as first argument (e.g. \"1.0.3\"). Aborting!${ESC_RESET}"
	exit 1
fi

changeListPath="${SCRIPT_PATH}/${SCRIPT_NAME}-${toVersion}.md"
echo -e "Writing change list to: ${changeListPath}"
echo -e "Determining changed files between version <${fromVersion}> to version <"${toVersion}">..."
echo ""
thishost=$(hostname -f) 
user=$(whoami)
echo "" | tee -a ${changeListPath}
echo -e "> This change list has been auto-generated on \`${thishost}\` by \`${user}\` with \`$(basename $0)\` on the $(date +%Y-%m-%d) at $(date +%H:%M:%S)." | tee -a ${changeListPath}
echo "" | tee -a ${changeListPath}

while read repo; do
	repoPath=${SCRIPT_PATH}/../${repo}
	cd ${repoPath}

	if git rev-parse "${repo}-${toVersion}^{tag}" >/dev/null 2>&1 ;	then
		filePaths=$(git diff --name-status "${repo}-${fromVersion}".."${repo}-${toVersion}" | sort | uniq )
		
		git rev-parse "${repo}-${fromVersion}^{tag}" >/dev/null 2>&1
		if (($? > 0)); then
			beginning=$(git rev-list HEAD | tail -n 1 )
			filePaths=$( git diff --name-status "${beginning}".."${repo}-${toVersion}" | sort | uniq )
		fi

		if [ ! -z "${filePaths}" ]; then
			echo -e "## Change list &lt;${repo}&gt; (version ${toVersion})" | tee -a ${changeListPath}
			echo "" | tee -a ${changeListPath}
			while read -r line ; do
				elements=($line)
				status=${elements[0]}
				filePath=${elements[1]}
				fileName="$(basename $filePath)"
				fileLink="https://bitbucket.org/${VENDOR}/$repo/src/${repo}-${toVersion}/${filePath}"

				# BEGIN JAVADOC -->
				tmpFilePath=${filePath}
				javadocText=""
				if [[ "$fileName" == *.java ]] && [[ "$fileName" != *Test.java ]] && [[ "$fileName" != "package-info.java" ]] && [[ "$filePath" != *"/test/"* ]] ; then
					if [[ "$tmpFilePath" == ${repo}* ]] ; then
						package="${tmpFilePath/'src/main/java/'}"
						package="$(dirname $package)"
						package="$(echo -e "$package" | sed 's|^[^/]*||')"
						package="$(dirname $package)"
						package="${package//\//.}"
						package="${package:1}"
						# JDK 15 format ?!?: javadocPath="${tmpFilePath/'src/main/java'/${toVersion}\/${package}}"
						javadocPath="${tmpFilePath/'src/main/java'/${toVersion}}"
						# Remove ".java":				
						javadocPath="${javadocPath%.*}"
						javadocLink="https://www.javadoc.io/doc/${GROUP}/${javadocPath}.html"
					else
						package="${tmpFilePath/'src/main/java/'}"
						package="$(dirname $package)"
						package="${package//\//.}"
						javadocPath="${tmpFilePath/'src/main/java/'/}"
						# Remove ".java":				
						javadocPath="${javadocPath%.*}"
						# JDK 15 format ?!?: javadocLink="https://www.javadoc.io/doc/${GROUP}/$repo/${toVersion}/${package}/${javadocPath}.html"
						javadocLink="https://www.javadoc.io/doc/${GROUP}/$repo/${toVersion}/${javadocPath}.html"
					fi
					javadocText="(see Javadoc at [\`$fileName\`](${javadocLink}))"
				fi
				# <-- END JAVADOC

				if [ "$status" == "A" ] ; then
					echo -e "* \[<span style=\"color:blue\">ADDED</span>\] [\`$fileName\`]($fileLink) ${javadocText}" | tee -a ${changeListPath}
				fi
				if [ "$status" == "M" ] ; then
					echo -e "* \[<span style=\"color:green\">MODIFIED</span>\] [\`$fileName\`]($fileLink) ${javadocText}" | tee -a ${changeListPath}
				fi
				if [ "$status" == "D" ] ; then
					echo -e "* \[<span style=\"color:red\">DELETED</span>\] \`$fileName\`" | tee -a ${changeListPath}
				fi
			done <<< "${filePaths}"
		fi
		echo "" | tee -a ${changeListPath}
	fi
done < "${REACTOR_PATH}"
cd "${CURRENT_PATH}"
changeList="$(awk -v RS= -v ORS='\n\n' '1' ${changeListPath})"
echo -e "${changeList}" > ${changeListPath}

# Create blog entry |-->
blogPath="${SCRIPT_PATH}/../../pro.metacodes/metacodes-blog/_posts/refcodes"
fileDate=$(date +%Y-%m-%d)
blogDate=$(date +%Y-%m-%dT%H:%M)
blogFilePath="${blogPath}/${fileDate}-refcodes.org_change_list_versions-${toVersion}.md"
templateFilePath="${SCRIPT_PATH}/changelist-blog.template"
cat "${templateFilePath}" "${changeListPath}" > "${blogFilePath}"
sed -i -e "s/###DATE###/${blogDate}/g" "${blogFilePath}"
sed -i -e "s/###VERSION###/${toVersion}/g" "${blogFilePath}"
# Create blog entry <--|

echo -e "\n> Done.\n"