# /////////////////////////////////////////////////////////////////////////////
# REFCODES.ORG
# =============================================================================
# This code is copyright (c) by Siegfried Steiner, Munich, Germany and licensed
# under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
# licenses:
# =============================================================================
# GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
# =============================================================================
# Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
# =============================================================================
# Please contact the copyright holding author(s) of the software artifacts in
# question for licensing issues not being covered by the above listed licenses,
# also regarding commercial licensing models or regarding the compatibility
# with other open source licenses.
# /////////////////////////////////////////////////////////////////////////////
#!/bin/bash

# ------------------------------------------------------------------------------
# INIT:
# ------------------------------------------------------------------------------

CURRENT_PATH="$(pwd)"
SCRIPT_PATH="$(dirname $0)"
cd "${SCRIPT_PATH}"
SCRIPT_PATH="$(pwd)"
cd "${CURRENT_PATH}"
SCRIPT_DIR="${SCRIPT_PATH##*/}"
SCRIPT_NAME="$(basename $0 .sh)"
PARENT_PATH="$(realpath $(dirname $0)/..)"
PARENT_DIR="${PARENT_PATH##*/}"
figlet -w 240 "/${PARENT_DIR}:>>>${SCRIPT_NAME}..."

# ------------------------------------------------------------------------------
# MAIN:
# ------------------------------------------------------------------------------

CONF_PATH="${SCRIPT_PATH}/meta.conf"
REACTOR_PATH=${SCRIPT_PATH}/reactor.txt
GROUP=$PARENT_DIR
VENDOR="${SCRIPT_DIR/-meta/}"
if [ -f "${CONF_PATH}" ]; then
  . ${CONF_PATH}
fi

function usage {
	echo -e "Usage: $SCRIPT_NAME.sh <v>"
	echo -e "    v: The Java version to use, e.g. 8, 9, 10, 11, 12, 13 and so on"
}

if [ $# -ne 1 ]; then
	usage
	echo -e "> Error! Expecting one argument!"
    exit 1
fi

version="${1}"

echo -e "> Changing Java versions to <${version}>..."

while read repo; do
	if [[ $repo != \#* ]] ; then
		# echo -e "> Processing artifact <${repo}>."
		for path in $(find "${PARENT_PATH}/${repo}" -type f -name pom.xml); do 
			parent="$(dirname $path)"
			if grep -q "<java.source.version>" "${path}"; then
				sed -i -e 's/\(<java.source.version>\).*\(<\/java.source.version>\)/<java.source.version>'${version}'<\/java.source.version>/g' ${path}
				if [ $? -eq 0 ]; then
					echo -e "> Changed Java source version for <${path//${PARENT_PATH}\/}> to <${version}>..."
				fi
			fi
			if grep -q "<java.target.version>" "${path}"; then
				sed -i -e 's/\(<java.target.version>\).*\(<\/java.target.version>\)/<java.target.version>'${version}'<\/java.target.version>/g' ${path}
				if [ $? -eq 0 ]; then
					echo -e "> Changed Java target version for <${path//${PARENT_PATH}\/}> to <${version}>..."
				fi
			fi
		done
	fi
done < "${REACTOR_PATH}"

cd "${CURRENT_PATH}"
echo -e "> Done."